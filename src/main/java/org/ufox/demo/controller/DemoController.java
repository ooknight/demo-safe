package org.ufox.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class DemoController {

    @RequestMapping("/demo")
    public Map<String, Object> demo() {
        Map<String, Object> r = new HashMap<>();
        r.put("message", "ok");
        return r;
    }

}
